import boto3
import os
import json 

bucket_name = os.environ.get('S3_UPLOAD_BUCKET_NAME')

def lambda_handler(event, context):
    file_name = event['queryStringParameters']['fileName']
    s3 = boto3.client('s3')
    key_name = file_name
    s3.upload_file(file_name, bucket_name, key_name)

    return {
        'statusCode': 200,
        'headers': { 'Content-Type': 'application/json' },
        'body': json.dumps({'message': 'Image Uploaded'})
      }
